#Compiler
CC = gcc
CFLAGS = -Wall -Werror -Wextra -std=c99 -pedantic
LDFLAGS = -lasan -fsanitize=address

#Debug
DBG = -g

#Source files
SRC = ${wildcard src/*.c}
OBJ = ${SRC:.c=.o}

#Test
CRI = -lcriterion
SRC_TEST = ${wildcard tests/*.c}
OBJ_TEST = ${SRC_TEST:.c=.o} ${filter-out src/my_find.o, $(OBJ)}

all: myfind

myfind: $(OBJ)
	$(CC) $(CFLAGS) $(SRC) -o myfind


#debug and test rules

mem: $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) $(SRC) -o mem_check

dbg: $(OBJ)
	$(CC) $(CFLAGS) $(DBG) $(SRC) -o dbg_check

check: $(OBJ_TEST)
	$(CC) $(CFLAGS) $(CRI) -o testsuite $^
	./testsuite

clean:
	$(RM) $(OBJ) $(OBJ_TEST) myfind mem_check dbg_check testsuite
