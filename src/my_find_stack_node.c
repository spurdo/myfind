#include "include/my_find_struct.h"

struct stack_node *init_stack_node(void)
{
    struct stack_node *stack = calloc(1, sizeof(struct stack_node));

    if (stack == NULL)
        return NULL;

    stack->size = 0;

    return stack;
}

void stkpushnode(struct stack_node *stack, struct ast_tree *node)
{
    if (stack->size < STACK_MAX)
        stack->node_stack[stack->size++] = node;
}

struct ast_tree *stk_pop_node(struct stack_node *stack)
{
    if (stack->size == 0)
        return NULL;

    struct ast_tree *item = stack->node_stack[--stack->size];
    return item;
}

int stack_node_empty(struct stack_node *stack)
{
    return (stack->size == 0);
}

static void rec_free(struct ast_tree *node)
{
    if (node == NULL)
        return;

    rec_free(node->left);
    rec_free(node->right);

    if (node->type < TYPE_OR)
        free(node->val);

    free(node);
}

void destroy_stack_node(struct stack_node *stack)
{
    rec_free(stack->node_stack[0]);
    free(stack);
}
