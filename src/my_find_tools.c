#include "include/my_find_tools.h"

#include <string.h>

int is_regular_file(char *path_name)
{
    struct stat path_stat;
    stat(path_name, &path_stat);
    return S_ISREG(path_stat.st_mode);
}

size_t expression_len(char **str)
{
    size_t i = 0;
    size_t size = 0;

    while (str[i] != NULL && str[i][0] != '-')
        i++;

    while (str[i] != NULL)
    {
        size++;
        i++;
    }

    return size;
}

void remove_slash(char *arg)
{
    if (arg[0] == '/' && arg[1] == '\0')
        return;

    size_t i = 1;

    while (arg[i + 1] != '\0')
    {
        i++;
    }

    if (arg[i] == '/' && arg[i + 1] == '\0')
        arg[i] = '\0';
}