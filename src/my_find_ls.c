#define _DEFAULT_SOURCE
#define _POSIX_SOURCE

#include "include/my_find_ls.h"

#include <err.h>
#include <errno.h>
#include <fnmatch.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "include/my_find_tools.h"

struct mini_expr
{
    enum token_type type;
    int (*fun)(char *, char *, unsigned char);
};

static void set_name(char *tmp, char *tok_val)
{
    size_t end = strlen(tok_val) - 1;
    size_t tmp_pos = 0;

    while (end > 0 && tok_val[end - 1] != '/')
        end--;

    while (tok_val[end] != '\0')
        tmp[tmp_pos++] = tok_val[end++];

    tmp[tmp_pos] = '\0';
}

static int name(char *tok_val, char *pattern, unsigned char d_type)
{
    d_type = d_type;

    char tmp[4096] = { 0 };

    set_name(tmp, tok_val);

    return !fnmatch(pattern, tmp, 0);
}

static unsigned char convert_to_dtype(char a)
{
    unsigned char res = 0;

    switch (a)
    {
    case 'b':
        return DT_BLK;
    case 'c':
        return DT_CHR;
    case 'd':
        return DT_DIR;
    case 'f':
        return DT_REG;
    case 'l':
        return DT_LNK;
    case 'p':
        return DT_FIFO;
    case 's':
        return DT_SOCK;
    default:
        errx(1, "Unknown type '%c'", a);
        break;
    }

    return res;
}

static int type(char *file_name, char *tok_val, unsigned char d_type)
{
    file_name = file_name;
    return (convert_to_dtype(tok_val[0]) == d_type);
}

static int newer(char *file_name, char *tok_val, unsigned char d_type)
{
    d_type = d_type;
    struct stat file_stat;
    struct stat tok_stat;

    stat(file_name, &file_stat);
    stat(tok_val, &tok_stat);

    return (tok_stat.st_mtime < file_stat.st_mtime);
}

int exec_match(struct ast_tree *root, char *file_name, unsigned char d_type)
{
    int flag = 0;

    struct mini_expr expr[3] = { { .type = TYPE_NAME, .fun = name },
                                 { .type = TYPE_TYPE, .fun = type },
                                 { .type = TYPE_NEWER, .fun = newer } };

    if (root->type == TYPE_AND)
        return exec_match(root->left, file_name, d_type)
            && exec_match(root->right, file_name, d_type);

    else if (root->type == TYPE_OR)
        return exec_match(root->left, file_name, d_type)
            || exec_match(root->right, file_name, d_type);

    else
    {
        for (size_t i = 0; i < 3; i++)
        {
            if (expr[i].type == root->type)
            {
                flag |= expr[i].fun(file_name, root->val, d_type);
                break;
            }
        }
    }

    return flag;
}

int list_dir(char *dir, struct ast_tree *ast, int first, struct dirent *curr)
{
    DIR *curr_dir = opendir(dir);

    if (curr_dir == NULL && !is_regular_file(dir))
    {
        warnx("%s: Permission denied", dir);
        return 1;
    }

    if (first)
    {
        if (ast == NULL || exec_match(ast, dir, DT_DIR))
            printf("%s\n", dir);

        if (is_regular_file(dir))
        {
            closedir(curr_dir);
            return 0;
        }
    }

    if (dir[strlen(dir) - 1] == '/')
        dir[strlen(dir) - 1] = '\0';

    while ((curr = readdir(curr_dir)))
    {
        if (strcmp("..", curr->d_name) && strcmp(".", curr->d_name))
        {
            char sub_dir[BUF_SIZE] = { 0 };
            strcat(sub_dir, dir);

            if (dir[0] != '/' || dir[1] != '\0')
                strcat(sub_dir, "/");

            strcat(sub_dir, curr->d_name);

            if (ast == NULL || exec_match(ast, sub_dir, curr->d_type))
                printf("%s/%s\n", dir, curr->d_name);

            if (curr->d_type == DT_DIR)
                list_dir(sub_dir, ast, 0, curr);
        }
    }

    closedir(curr_dir);

    return 0;
}