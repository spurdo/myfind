#include "include/my_find_struct.h"

struct queue *init_queue(void)
{
    struct queue *queue = calloc(1, sizeof(struct queue));

    if (queue == NULL)
        return NULL;

    queue->head = NULL;
    queue->tail = NULL;

    return queue;
}

void enqueue(struct queue *queue, struct token *tok)
{
    struct elt *e = calloc(1, sizeof(struct elt));

    e->data = tok;
    e->next = NULL;

    if (queue->head == NULL)
        queue->head = e;

    else
        queue->tail->next = e;

    queue->tail = e;
}

struct token *dequeue(struct queue *queue)
{
    struct elt *tmp;
    struct token *res;

    res = queue->head->data;
    tmp = queue->head;

    if (queue->head == queue->tail)
    {
        queue->tail = NULL;
        queue->head = NULL;
    }

    else
        queue->head = tmp->next;

    free(tmp);

    return res;
}

int queue_empty(struct queue *queue)
{
    return (queue->head == NULL);
}

void destroy_queue(struct queue *queue)
{
    while (!queue_empty(queue))
        free(dequeue(queue));

    free(queue);
}
