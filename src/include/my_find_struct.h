#ifndef MY_FIND_STRUCT_H
#define MY_FIND_STRUCT_H

#include <stddef.h>
#include <stdlib.h>

#include "my_find_parse.h"

#define STACK_MAX 4096

struct elt
{
    struct token *data;
    struct elt *next;
};

struct queue
{
    struct elt *head;
    struct elt *tail;
};

struct stack
{
    struct token *tok_stack[STACK_MAX];
    size_t size;
};

struct stack_node
{
    struct ast_tree *node_stack[STACK_MAX];
    size_t size;
};

struct queue *init_queue(void);
struct stack *init_stack(void);
struct stack_node *init_stack_node(void);

void enqueue(struct queue *queue, struct token *tok);
void stkpush(struct stack *stack, struct token *tok);
void stkpushnode(struct stack_node *stack, struct ast_tree *node);

struct token *dequeue(struct queue *queue);
struct token *stk_pop(struct stack *stack);
struct ast_tree *stk_pop_node(struct stack_node *stack);

int queue_empty(struct queue *queue);
int stack_empty(struct stack *stack);
int stack_node_empty(struct stack_node *stack);

void destroy_queue(struct queue *queue);
void destroy_stack(struct stack *stack);
void destroy_stack_node(struct stack_node *stack);

#endif