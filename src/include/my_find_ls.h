#ifndef MY_FIND_LS_H
#define MY_FIND_LS_H

#include <dirent.h>
#include <stdio.h>
#include <sys/types.h>

#include "my_find_struct.h"

int list_dir(char *dir, struct ast_tree *ast, int first, struct dirent *curr);
int exec_match(struct ast_tree *root, char *file_name, unsigned char d_type);

#endif