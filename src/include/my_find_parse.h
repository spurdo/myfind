#ifndef MY_FIND_PARSE_H
#define MY_FIND_PARSE_H

#include "my_find_tools.h"

enum token_type
{
    TYPE_NAME = 0,
    TYPE_TYPE,
    TYPE_NEWER,
    TYPE_OR,
    TYPE_AND
};

struct token_expr
{
    char *str;
    enum token_type type;
};

struct token
{
    enum token_type type;
    char *val;
};

struct token_list
{
    struct token *list[BUF_SIZE];
    size_t size;
};

struct ast_tree
{
    char *val;
    enum token_type type;

    struct ast_tree *left;
    struct ast_tree *right;
};
struct token_list *init_tokens();
struct token_list *parse_token(char **str, size_t start, int *failed);
struct stack_node *shunting_yard(struct token_list *list, int *err);

#endif