#ifndef MY_FIND_TOOLS_H
#define MY_FIND_TOOLS_H

#define BUF_SIZE 4096

#include <err.h>
#include <errno.h>
#include <fnmatch.h>
#include <stddef.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "my_find_parse.h"

int is_regular_file(char *path_name);
size_t expression_len(char **str);
void remove_slash(char *arg);

#endif