#define _GNU_SOURCE

#include "include/my_find_parse.h"

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "include/my_find_struct.h"
#include "include/my_find_tools.h"

static struct token_expr exp[] = { { "-name", TYPE_NAME },
                                   { "-type", TYPE_TYPE },
                                   { "-newer", TYPE_NEWER },
                                   { "-a", TYPE_AND },
                                   { "-o", TYPE_OR } };

struct token_list *init_tokens()
{
    struct token_list *list = calloc(1, sizeof(struct token_list));

    list->size = 0;

    return list;
}

static struct token_list *check_error(char *str, struct token *tok,
                                      struct token_list *l, int *err)
{
    if (str == NULL || !strcmp(str, "-a") || !(strcmp(str, "-o")))
    {
        *err = 1;
        free(tok);
    }
    return l;
}

struct token_list *parse_token(char **str, size_t start, int *err)
{
    size_t lpos = 0;
    struct token_list *l = init_tokens();

    for (int i = start; str[i] != NULL; i++)
    {
        int seen = 0;
        struct token *tok = calloc(1, sizeof(struct token));

        for (unsigned j = 0; j < sizeof(exp) / sizeof(*exp); ++j)
        {
            if (!strcmp(str[i], exp[j].str))
            {
                seen = 1;
                tok->type = exp[j].type;

                if (tok->type < TYPE_OR)
                {
                    l = check_error(str[i + 1], tok, l, err);

                    if (*err)
                        return l;

                    tok->val = strdup(str[++i]);
                }

                break;
            }
        }

        if (!seen)
        {
            free(tok);
            warnx("Unknown token: '%s'", str[i]);
            *err = 1;
            break;
        }

        l->list[lpos++] = tok;
        l->size += 1;
    }

    return l;
}

static struct stack_node *polish_to_ast(struct queue *out_queue, int *err)
{
    struct stack_node *stack_node = init_stack_node();

    for (struct elt *e = out_queue->head; e != NULL; e = e->next)
    {
        struct ast_tree *node = calloc(1, sizeof(struct ast_tree));

        if (e->data->type < TYPE_OR)
        {
            node->type = e->data->type;
            node->val = e->data->val;

            stkpushnode(stack_node, node);
        }

        else
        {
            struct ast_tree *child_1 = stk_pop_node(stack_node);
            struct ast_tree *child_2 = stk_pop_node(stack_node);

            if (child_1 == NULL || child_2 == NULL)
            {
                free(node);
                destroy_queue(out_queue);
                *err = 1;
                return stack_node;
            }

            node->val = NULL;
            node->type = e->data->type;

            node->left = child_1;
            node->right = child_2;

            stkpushnode(stack_node, node);
        }
    }

    destroy_queue(out_queue);

    return stack_node;
}

struct stack_node *shunting_yard(struct token_list *list, int *err)
{
    if (list == NULL)
        return NULL;

    int is_implicit = 0;
    struct stack *op_stack = init_stack();
    struct queue *out_queue = init_queue();

    for (size_t i = 0; i < list->size; i++)
    {
        if (is_implicit && list->list[i]->type < TYPE_OR)
        {
            struct token *tmp = calloc(1, sizeof(struct token));
            tmp->type = TYPE_AND;
            stkpush(op_stack, tmp);
            is_implicit = 0;
        }

        if (!is_implicit && list->list[i]->type < TYPE_OR)
        {
            enqueue(out_queue, list->list[i]);
            is_implicit = 1;
        }

        else
        {
            is_implicit = 0;

            while (!stack_empty(op_stack)
                   && op_stack->tok_stack[op_stack->size - 1]->type
                       > list->list[i]->type)
                enqueue(out_queue, stk_pop(op_stack));

            stkpush(op_stack, list->list[i]);
        }
    }

    while (!stack_empty(op_stack))
        enqueue(out_queue, stk_pop(op_stack));

    destroy_stack(op_stack);

    return polish_to_ast(out_queue, err);
}
