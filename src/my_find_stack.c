#include "include/my_find_struct.h"

struct stack *init_stack(void)
{
    struct stack *stack = calloc(1, sizeof(struct stack));

    if (stack == NULL)
        return NULL;

    stack->size = 0;

    return stack;
}

void stkpush(struct stack *stack, struct token *tok)
{
    if (stack->size < STACK_MAX)
        stack->tok_stack[stack->size++] = tok;
}

struct token *stk_pop(struct stack *stack)
{
    return stack->tok_stack[--stack->size];
}

int stack_empty(struct stack *stack)
{
    return (stack->size == 0);
}

void destroy_stack(struct stack *stack)
{
    for (size_t i = 0; i < stack->size; i++)
        free(stack->tok_stack[i]);
    free(stack);
}
