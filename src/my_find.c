#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "include/my_find_ls.h"
#include "include/my_find_parse.h"
#include "include/my_find_tools.h"

static void destroy_list(struct token_list *list)
{
    for (size_t i = 0; i < list->size; i++)
    {
        if (list->list[i]->type < TYPE_OR)
            free(list->list[i]->val);
        free(list->list[i]);
    }

    free(list);
}

static int get_expr_start_pos(char **argv)
{
    int pos = 1;

    while (argv[pos] != NULL && argv[pos][0] != '-')
        pos++;

    return pos;
}

static int run_find(char **argv, int ex_st, struct token_list *list, int flag)
{
    int err = 0;
    struct dirent dir;
    struct ast_tree *node = NULL;
    struct stack_node *ast_stack = shunting_yard(list, &err);

    if (err)
    {
        free(list);
        destroy_stack_node(ast_stack);
        warnx("Invalid AST syntax!");
        return 1;
    }

    if (ast_stack != NULL)
        node = ast_stack->node_stack[0];

    if (argv[1] == NULL || argv[1][0] == '-')
        flag = list_dir(".", node, 1, &dir);

    else
    {
        for (int i = 1; i < ex_st; i++)
        {
            if (access(argv[i], F_OK) == -1)
                warnx("File %s doesn't exist.", argv[i]);
            else
                flag |= list_dir(argv[i], node, 1, &dir);
        }
    }

    destroy_stack_node(ast_stack);
    free(list);

    return flag;
}

int main(int argc, char **argv)
{
    if (argc == 0)
        return 1;

    int err = 0;
    int expr_start = get_expr_start_pos(argv);
    struct token_list *list = parse_token(argv, expr_start, &err);

    if (err)
    {
        destroy_list(list);
        warnx("Invalid syntax! Could not parse tokens!");
        return 1;
    }

    return run_find(argv, expr_start, list, 0);
}
